//help: https://github.com/sarahgp/p5bots/tree/master/examples#variable-resistors
//server starten mit bots-go -d Pfad-zu-Verzeichnis-mit-index.html
'use strict';

var b = p5.board('COM4', 'arduino');
var poti;


function setup(){
  //docu der Methoden: https://github.com/sarahgp/p5bots/tree/master/src/client#vres-methods
  poti = b.pin(0, 'VRES'); //an A0 angeschlossen
  poti.read(handlePotiInput); //nur im setup, darf nur einmal definiert werden!
  poti.range([0, 400]);
  createCanvas(800, 400);  
}

function draw(){
	rect(10, 10, poti.val, poti.val);
}

function handlePotiInput(val){
  //console.log(val);
  
}
