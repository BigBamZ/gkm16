'use strict';

function Mario() {
	var jumpHeight = 200;
	var size = 50;
	this.box = new Box(50, GROUNDLEVEL, size, size);
	var velocityY = 0;
	this.jump = function () {
		console.log("jump");
		velocityY = -2;
	}

	this.draw = function () {
		this.box.y = this.box.y + velocityY;
		if (this.box.y < jumpHeight)
			velocityY = 4;
		if (this.box.y >= GROUNDLEVEL)
			velocityY = 0;
		image(images.mario, this.box.left(), this.box.top(), size, size);
	}
}
