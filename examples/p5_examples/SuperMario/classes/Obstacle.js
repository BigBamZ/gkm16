'use strict';

function Obstacle(){
	this.box = new Box(random(width, 2*width), GROUNDLEVEL, images.obstacle.width, images.obstacle.height); 
	this.draw = function () {
		this.box.x = this.box.x - 1;
		if (this.box.x < 0) 
			this.box.x = random(width, 2*width);
		image(images.obstacle, this.box.left(), this.box.top());
	}
}