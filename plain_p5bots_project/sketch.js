'use strict';

// Board setup — you may need to change the port
var b = p5.board('/dev/cu.usbmodemFA131', 'arduino');

// Test Read & Threshold
var pmeter;
var potiValue = 0;


function setup() {

  createCanvas(800, 800);


  pmeter = b.pin(0, 'VRES');
  pmeter.read(function(val){
    clear();
    potiValue = val;
  });

  pmeter.range([10, 400]);
  pmeter.threshold(600);


}

function draw() {
  ellipse(300, 300, potiValue, potiValue);

}

function keyPressed() {
  console.log('key pressed!')
}

function mousePressed() {
  console.log('mouse pressed!')

}